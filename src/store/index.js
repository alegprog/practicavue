import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})

export default new Vuex.Store({
  state: {
    authenticated: false,
    user: {}
  },
  getters: {
    getAuthenticated(state){
      return state.authenticated
    },
    getUser(state){
        return state.user
    }
  },
  mutations: {
    SET_AUTHENTICATED (state, value) {
      state.authenticated = value
    },
    SET_USER (state, value) {
        state.user = value
    }
  },
  actions: {
    fnLogin({ commit },data){
        commit("SET_USER", data.info);
        commit("SET_AUTHENTICATED", data.status);
    },
    fnLogout({ commit }){
      commit("SET_USER", {});
      commit("SET_AUTHENTICATED", false);
    }
  },
  modules: {
  },
  plugins: [vuexLocal.plugin]
})
