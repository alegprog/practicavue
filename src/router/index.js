import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import CrudView from '../views/CrudView.vue'
import CrudAddView from '../views/CrudAddView.vue'
import CrudEditView from '../views/CrudEditView.vue'
import LoginView from '../views/LoginView.vue'
import RegisterView from '../views/RegisterView.vue'
import PrivateView from '../views/PrivateView.vue'
import ProfileView from '../views/ProfileView.vue'
import store from '../store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
    meta:{
      middleware:"public",
    }
  },
  {
    path: '/crud',
    name: 'crud',
    component: CrudView,
    meta:{
      middleware:"public",
    }
  },
  {
    path: '/crud/add',
    name: 'crudAdd',
    component: CrudAddView,
    meta:{
      middleware:"public",
    }
  },
  {
    path: '/crud/edit/:id',
    name: 'crudEdit',
    component: CrudEditView,
    meta:{
      middleware:"public",
    }
  },
  {
    path: '/login',
    name: 'login',
    component: LoginView,
    meta:{
      middleware:"guest",
    }
  },
  {
    path: '/register',
    name: 'register',
    component: RegisterView,
    meta:{
      middleware:"guest",
    }
  },
  {
    path: '/private',
    name: 'private',
    component: PrivateView,
    meta:{
      middleware:"auth",
    }
  },
  
  {
    path: '/profile',
    name: 'profile',
    component: ProfileView,
    meta:{
      middleware:"auth",
    }
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue'),
    meta:{
      middleware:"public",
    }
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  //document.title = `${to.meta.title} - ${process.env.MIX_APP_NAME}`
  if(to.meta.middleware=="guest"){
      if(store.state.authenticated){
          next({name:"home"})
      }
      next()
  }if(to.meta.middleware=="public"){
    next()
  }else{
      if(store.state.authenticated){
          next()
      }else{
        console.log('to',to);
        if(to.name != 'register'){
          next({name:"login"})
        }
      }
  }
})

export default router
// ..
